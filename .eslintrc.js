module.exports = {
  plugins: ['vue'], // enable vue plugin
  extends: ['plugin:vue/essential', 'prettier'], // activate vue related rules
  rules: {
    semi: 0,
    indent: ["error", "tab"]
  }
}