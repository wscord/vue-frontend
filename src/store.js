import Vue from 'vue'
import Vuex from "vuex"
import { router } from "./router"
import { showDialog, hideDialog } from "./notifications"

Vue.use(Vuex)

var store = new Vuex.Store({
	state: {
		socket: null,
		guilds: [],
		messages: [],
		challenges: [],
		answers: [],
	},
	mutations: {}
})

store.state.socket = new WebSocket(`ws://localhost:8080/api/ws`, store.state.answers);

store.state.socket.addEventListener("error", (event) => {
	try {
		let payload = JSON.parse(event.data)
		for (i=0; i < payload.data.count; i++) {
			store.state.challenges.push({
				name: "Field " + i,
				id: i,
				value: ""
			})
		}
	} catch(e) {
		showDialog(e)
	}
	
	router.push("/login")
})

store.state.socket.addEventListener("message", (event) => {
	try {
		let payload = JSON.parse(event.data)
		switch(payload.type) {
		case "bulk_msg":
			for (let [id, msg] of Object.entries(payload.data.messages)) {
				let m = msg
				/**/m.id = id

				store.state.messages.unshift(m)
			}

			break
		case "edit":
			let id = Object.keys(payload.data.messages)[0]
			let msg = payload.data.messages[id]
			store.state.messages.forEach(m => {
				console.log(id, msg, m)
				if (id == m.id) {
					m.content = msg.content
					return
				}
			})

			break
		case "initial":
			try {
				store.state.guilds = payload.data.servers
			} catch(e) {
				showDialog(e)
			}

			break
		case "error":
			// switch(payload.data) {
			// case "Unknown error, check console", "Session expired", "Nil pointer", "Wrong data type", "Channel is not initiated", "Discord is not ready.":
			showDialog(payload.data)
			// default:
			// 	console.log("Got error:", typeof(payload.data), payload.data)
			// 	let m = {
			// 		author: "<-SYSTEM",
			// 		color: "#AAAAAA",
			// 		content: payload.data,
			// 		id: 0,
			// 	}

			// 	store.state.messages.unshift(m)
			// }

		// 	break
		}
	} catch (e) {
		showDialog(e)
	}

	try {
		let msgElem = document.getElementById("messages")
		/**/msgElem.scrollTop = msgElem.scrollHeight
	} catch(e) {
		console.log(e)
	}
});

store.state.socket.addEventListener("close", (event) => {
	showDialog("Websocket closed unexpectedly")
})

export default store