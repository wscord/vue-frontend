let showDialog = (msg) => {
	document.getElementById("popup-dialog").style.display = "flex"
	document.getElementById("popup-content").innerText = msg
}

let hideDialog = () => {
	document.getElementById('popup-dialog').style.display = 'none'
}

export {
	showDialog, hideDialog	
}