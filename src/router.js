import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Guilds from './components/Guilds.vue'
import Messages from './components/Messages.vue'
import store from "./store"

Vue.use(Router)

let router = new Router({
	routes: [
		{
			path: '/login',
			name: 'login',
			component: Login,
			store,
		},
		{
			path: '/guilds',
			name: 'guilds',
			component: Guilds,
			store,
		},
		{
			path: '/messages/:id',
			name: 'messages',
			component: Messages,
			store,
		}
	]
})

router.replace({
	path: '*',
	redirect: '/guilds',
})

export default router