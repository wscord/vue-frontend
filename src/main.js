import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from "./store"
import { showDialog, hideDialog } from "./notifications"

Vue.config.productionTip = true

Vue.prototype.$helpers = {
	dialog: showDialog,
	hide: hideDialog,
}

const v = new Vue({
	router,
	store,
	render: h => {
		return h(App)
	}
})

v.$mount('#app')
